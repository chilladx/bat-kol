#!/usr/bin/env python
# -*- coding: utf8 -*-

from langchain_openai import OpenAI
from pandasai import SmartDataframe
from pandasai.llm.openai import OpenAI as pdai_openai
import openai


OPENAI_TEMPERATURE = 0.7


class ChatResponse:
    def __init__(self, message: str, error: bool = False):
        self.error = error
        self.message = message


class ChatDummy:
    def __init__(self) -> None:
        self.config = {}
        self.state_ready = True

    def chat(self, context):
        """Send a message to the ChatBot, and retrieve answer

        Args:
            context (dict): Dict with the message

        Returns:
            ChatResponse: object with the response of the Chat
        """
        response = ChatResponse(
            message=f'Did you say "{context["message"]}"?', error=False
        )
        return response


class ChatOpenAI:
    def __init__(self, api_key) -> None:
        self.config = {}

        if not api_key.startswith("sk-"):
            self.state_ready = False
            return

        self.llm = OpenAI(
            temperature=OPENAI_TEMPERATURE, openai_api_key=api_key
        )
        self.config["Temperature"] = OPENAI_TEMPERATURE

        self.state_ready = True

    def chat(self, context):
        """Send a message to the ChatBot, and retrieve answer

        Args:
            context (dict): Dict with the message

        Returns:
            ChatResponse: object with the response of the Chat
        """
        try:
            response = ChatResponse(
                message=self.llm(context["message"]), error=False
            )
        except openai.RateLimitError:
            response = ChatResponse(
                message=(
                    "You hit the rate limit! Please upgrade your OpenAI"
                    " subscription."
                ),
                error=True,
            )
        return response


class ChatPandasAI:
    def __init__(self, api_key) -> None:
        self.config = {}

        if not api_key.startswith("sk-"):
            self.state_ready = False
            return

        self.llm = pdai_openai(api_token=api_key)
        self.config["LLM"] = "OpenAI"

        self.state_ready = True

    def chat(self, context):
        """Send a message to the ChatBot, and retrieve answer

        Args:
            context (dict): Dict with the message

        Returns:
            ChatResponse: object with the response of the Chat
        """
        sdf = SmartDataframe(context["df"], config={"llm": self.llm})
        try:
            response = ChatResponse(message=sdf.chat(context["message"]))
        except openai.RateLimitError:
            response = ChatResponse(
                message=(
                    "You hit the rate limit! Please upgrade your OpenAI"
                    " subscription."
                ),
                error=True,
            )
        return response
