#!/usr/bin/env python
# -*- coding: utf8 -*-

import streamlit as st
import time
import pandas as pd


AVAILABLE_LLM = {
    "openai": "OpenAI",
    "pandasai": "PandasAI (+ OpenAI)",
    "dummy": "Dummy LLM (repeat your prompt)",
}


def ask_api_key(message: str = ""):
    """Helper to ask for an API key

    Args:
        message: Text to introduce the block
    """
    ask_placeholder = st.empty()
    with ask_placeholder.form("api_config"):
        st.write(message)
        resp = st.text_input("API Key", type="password")
        submitted = st.form_submit_button("Submit")
        while not submitted:
            time.sleep(1)
    ask_placeholder.empty()
    ask_placeholder = None

    return resp


def display_message(message: dict = {}, error: bool = False):
    """Properly display a message in the chat, based on its type

    Args:
        message: Dict with 'role' and 'content' as keys
        error: boolean to define is message is an error message
    """
    with st.chat_message(message["role"]):
        if error:
            st.error(message["content"], icon="⚠")
        else:
            if isinstance(message["content"], str):
                st.write(message["content"])
            elif isinstance(message["content"], pd.DataFrame):
                st.dataframe(data=message["content"])


def ask_csv_upload(message: str = ""):
    """Helper to as for a file upload

    Args:
        message: Text to introduce the block
    """
    result = None

    csv_placeholder = st.empty()
    with csv_placeholder.form("file_upload"):
        result = st.file_uploader(message, type="csv")
        submitted = st.form_submit_button("Submit")
        while not submitted:
            time.sleep(1)
    csv_placeholder.empty()
    csv_placeholder = None

    return result


def choose_llm():
    """Display LLM selector and return result"""

    choose_placeholder = st.empty()
    with choose_placeholder.form("choose_llm"):
        option = st.selectbox(
            label="Choose your LLM",
            options=AVAILABLE_LLM.keys(),
            format_func=lambda x: AVAILABLE_LLM[x],
        )
        submitted = st.form_submit_button("Submit")
        while not submitted:
            time.sleep(1)
    choose_placeholder.empty()
    choose_placeholder = None

    return option
