# bat-kol

## Description

bat-kol is a front-end for your chat bot, that you can connect to multiple AI, such as OpenAI or other LLM.

## Configuration

In order to configure the application, you need to create a `.env` file, or to pass the configuration as environment variables. Here are the options:

| Key          | Description                                    | Default |
| ------------ | ---------------------------------------------- | ------- |
| app_title    | Title of the application page                  |         |
| app_info     | Synopsis of the application                    |         |
| first_phrase | First phrase from the ChatBot                  |         |
| llm          | The LLM to use ("openai", "pandasai", "dummy") | dummy   |

### LLM Specific Configuration

You can define configuration specific to the LLM you chose.

#### OpenAI

| Key            | Description                      | Default |
| -------------- | -------------------------------- | ------- |
| openai_api_key | OpenAI API Key with all accesses |         |

#### PandasAI (+ OpenAI)

| Key            | Description                      | Default |
| -------------- | -------------------------------- | ------- |
| openai_api_key | OpenAI API Key with all accesses |         |

## Usage

bat-kol is released in a container, so you can run the latest version using:

```sh
$ docker run -p 8501:8501 --env-file .env registry.gitlab.com/chilladx/bat-kol/bat-kol:main
```

Head to http://localhost:8501 to access the UI.

You can also head to the [registry](https://gitlab.com/chilladx/bat-kol/container_registry/), to find other available tags

Finally, you can use the [docker-compose.yml](docker-compose.yml) file to specify your options, or overides the default options.

## Installation

bat-kol is a python project, using poetry.

```sh
$ poetry shell
(venv)$ poetry install
```

To run the application, use the following commands:

```sh
(venv)$ streamlit run app.py
```

## Authors and acknowledgment

- This project has been initiated by chilladx <chilladx@pm.me>.
- The name of the project has been suggested by Chrales <charles.francoise@gmail.com>.

## License

bat-kol is licensed under [MIT License](LICENSE).
