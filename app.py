#!/usr/bin/env python
# -*- coding: utf8 -*-

import streamlit as st
from bat_kol.chat_engine import ChatDummy, ChatOpenAI, ChatPandasAI
from bat_kol.ui_utils import (
    ask_api_key,
    display_message,
    ask_csv_upload,
    choose_llm,
)
import pandas as pd
from dotenv import load_dotenv
import os


load_dotenv()
config = dict(os.environ)

st.set_page_config(
    page_title="bat-kol",
    layout="wide",
    initial_sidebar_state="auto",
    menu_items=None,
)
st.title(config.get("app_title", "Chat with bat-kol 💬"))
st.info(
    config.get(
        "app_info",
        (
            "Front-end for your chat bot, that you can connect to multiple AI,"
            "such as OpenAI or other LLM"
        ),
    )
)
st.markdown(
    r"""
    <style>
    .stDeployButton {
            visibility: hidden;
        }
    </style>
    """,
    unsafe_allow_html=True,
)

if "df" not in st.session_state.keys():
    st.session_state.df = None
if "llm_ready" not in st.session_state.keys():
    st.session_state.llm_ready = False

if "messages" not in st.session_state.keys():
    st.session_state.messages = [
        {
            "role": "assistant",
            "content": config.get(
                "first_phrase", "Go ahead, ask me a question!"
            ),
        }
    ]

if (
    "chat_engine" not in st.session_state.keys()
    or st.session_state.llm_ready is False
):
    if "chosen_llm" not in st.session_state.keys():
        if config.get("llm") is None:
            st.session_state.chosen_llm = choose_llm()
        else:
            st.session_state.chosen_llm = config.get("llm")

    if st.session_state.chosen_llm == "dummy":
        st.session_state.chat_engine = ChatDummy()
    elif st.session_state.chosen_llm == "openai":
        if config.get("openai_api_key") is None:
            config["openai_api_key"] = ask_api_key(
                message=(
                    "Can't find your OpenAI API key in configuration. "
                    "Please submit one:"
                )
            )

        st.session_state.chat_engine = ChatOpenAI(
            api_key=config.get("openai_api_key")
        )
        if not st.session_state.chat_engine.state_ready:
            st.warning("Please use a valid your OpenAI API key!", icon="⚠")
            st.stop()
    elif st.session_state.chosen_llm == "pandasai":
        if config.get("openai_api_key") is None:
            config["openai_api_key"] = ask_api_key(
                message=(
                    "Can't find your OpenAI API key in configuration. "
                    "Please submit one:"
                )
            )

        st.session_state.chat_engine = ChatPandasAI(
            api_key=config.get("openai_api_key")
        )
        if not st.session_state.chat_engine.state_ready:
            st.warning("Please use a valid your OpenAI API key!", icon="⚠")
            st.stop()
    else:
        st.error(
            (
                f"The LLM you configured ({st.session_state.chosen_llm})"
                f" is unknown"
            ),
            icon="⚠",
        )
        st.stop()
    st.session_state.llm_ready = st.session_state.chat_engine.state_ready

if (
    st.session_state.llm_ready
    and st.session_state.chosen_llm == "pandasai"
    and st.session_state.df is None
):
    if st.session_state.df is None:
        uploaded_file = ask_csv_upload(
            "Upload a CSV file to load in a DataFrame"
        )
        if uploaded_file is not None:
            df = pd.read_csv(uploaded_file)
            st.session_state.df = df

with st.sidebar:
    st.title("Current config")
    st.markdown(
        f"""
        * LLM: {st.session_state.chosen_llm}"""
    )
    st.title("LLM config")
    for k, v in st.session_state.chat_engine.config.items():
        st.markdown(
            f"""
            * {k}: {v}"""
        )


if prompt := st.chat_input("Your question"):
    st.session_state.messages.append({"role": "user", "content": prompt})

for message in st.session_state.messages:
    display_message(message=message)

if st.session_state.messages[-1]["role"] != "assistant":
    with st.spinner("Hold on a sec..."):
        response = st.session_state.chat_engine.chat(
            context={"message": prompt, "df": st.session_state.df}
        )

        display_message(
            message={"role": "assistant", "content": response.message},
            error=response.error,
        )

        st.session_state.messages.append(
            {"role": "assistant", "content": response.message}
        )
